var datas = {
	car : {
		3 : {
			coeff : 0.41,
		},
		4 : {
			coeff : 0.494,
		},
		5 : {
			coeff : 0.543,
		},
		6 : {
			coeff : 0.568,
		},
		7 : {
			coeff : 0.595,
		},
	},
	motorbike : {
		1 : {
			coeff : 0.338,
		},
		3 : {
			coeff : 0.4,
		},
		6 : {
			coeff : 0.518,
		},
	},
	cyclobike : {
		1 : {
			coeff : 0.338,
		},
	},
}



/**
 * Si je rentre une mauvaise valeur valeur ou que mon champ est vide => input passe en rouge
 * Detection des erreurs se fait à la sortie de mon champ
 * CV ==> Select en HTML
 * Calcul des frais se fait au clic sur "Calculer"
 * Le resultat ainsi que la formule employée doit se trouver dans un div qui aura la class total
 * A chaque changement de valeur, le total dans le html doit se reset
 */
$(document).ready(function(){



	$(".type").change(function(){
		var val = $(this).val();
		if (val == "car"){
			$("#cvlist").html("<select id='cv'name='cv'><option value='3'>3 or less</option><option value='4'>4</option><option value='5'>5</option><option value='6'>6</option><option value='7'>7 and more</option></select>")
		}else if (val == "motorbike"){
			$("#cvlist").html("<select id='cv'name='cv'><option value='1'>1 or 2</option><option value='3'>3, 4 et 5</option><option value='6'>6 et plus</option></select>")
		}else if (val == "cyclobike") {
		  $("#cvlist").html("<select id='cv'name='cv'><option value='1'>1 or 2</option></select>")
		}else if (val == "selection"){
			$("#cvlist").html("")
		}
	})

	$('form').on('submit', function(){
    var i = datas[$('.type').val()][$('#cv').val()].coeff
		var km = $('#km').val()
    var coeff = 0
    var total = 0
		var ar = $('#radio input:checked').val()
		var total2 = 0

		if(isNaN(km) || km < 0 || km == "" || km > 5000){
			$('#total').val(0)
			return false
		}

    total = km * i;

		if (ar === undefined){
			total2 = "erreur"
			return false
		}
		else if(ar == 3){
			total2 = total * 2
		}else{
			total2 = total
		}

		$('#total').val(total2 = total2.toFixed(2))



	})


	$('#km').on('blur', function(){
		var km = $('#km').val()

		if(isNaN(km) || km < 0 || km == "" || km > 5000){
			$(this).css('background','red')
			$('#total').val(0)
			return false
		}else{
			$(this).css('background','white')
		}
		$('#total').val(total2)
	})


	$("#km").on("keyup", function(){
	    $("#total").trigger('submit')
	})

	$("#choose, #cvlist, #go, #return, #ar").on("change", function(){
	    $("#total").trigger("submit")
	})

	$(function() {
	  $( "#datepicker" ).datepicker();
	  $('#timepicker').timepicker();
	})

})
